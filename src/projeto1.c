#include <stdio.h>
#include <stdlib.h>

#include "aux.h"

int main()
{
	FILE *fp;
	NETWORK *rede;

	fp = fopen("karate/karate.gml", "r");
	rede = (NETWORK *)malloc(sizeof(NETWORK));

	if (read_network(rede, fp) != EXIT_SUCCESS)
	{
		exit(EXIT_FAILURE);
	}

	fclose(fp);	

	printf("------ GRAFO --------\n");
	printf("- Possui %d vértices -\n", rede->nvertices);
	printf("---------------------\n");

	imprime_graus(rede);
	// bron_kerbosch(rede);
	destruir_rede(rede);

	return EXIT_SUCCESS;
}