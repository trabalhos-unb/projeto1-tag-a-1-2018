#include <stdio.h>
#include <stdlib.h>

#include "readgml/readgml.h"
#include "readgml/network.h"

#define MAX_DEGREE 17

int cmpfunc (const void *, const void *);

void construir_rede(const char *, NETWORK *);

void destruir_rede(NETWORK *);

void imprime_graus(NETWORK *);

void bron_kerbosch(NETWORK *);