#include "aux.h"

void algoritmo(NETWORK *, VERTEX *, VERTEX *, VERTEX *);
VERTEX *extrai_vertices_conexos(NETWORK *, VERTEX *);
VERTEX *adiciona_vertices(VERTEX *, VERTEX *);

int cmpfunc(const void *a, const void *b)
{
	return -(((VERTEX *)a)->degree - ((VERTEX *)b)->degree);
}

void destruir_rede(NETWORK *rede)
{
	free_network(rede);
	free(rede);
}

void imprime_graus(NETWORK *rede)
{
	qsort(rede->vertex, rede->nvertices, sizeof(VERTEX), cmpfunc);
	printf("Listagem de vértices em ordem decrescente: \n");
	for (int i = 0; i < rede->nvertices; i++)
	{
		printf("Vértice %d: %d\n", i + 1, rede->vertex[i].degree);
	}
}

void bron_kerbosch(NETWORK *rede)
{
	//
	algoritmo(rede, rede->vertex, 0, 0);
}

void algoritmo(NETWORK *rede, VERTEX *candidatos, VERTEX *clique,  VERTEX *analisados)
{
	// p = candidatos
	// c = vertices do clique
	// s = analisados
	VERTEX *c = clique;
	VERTEX *p = candidatos;
	VERTEX *s = analisados;

	//Se p for vazio e r for vazio, encontrou maximal
	if (p == NULL && s == NULL)
	{
		return;
	}
	//Se p for vazio encerra a execucao e faz backtraking
	if (p == NULL)
	{
		return;
	}

	//Adiciona vertice em p
	//Adiciona vizinhos em s e c
	adiciona_vertices(c, p);
	p = extrai_vertices_conexos(rede, p);
	algoritmo(rede, p, c, s);
}

VERTEX *extrai_vertices_conexos(NETWORK *rede, VERTEX *vertex)
{
	VERTEX *conexos = NULL;
	VERTEX *grafo = rede->vertex;
	EDGE *arestas = vertex->edge;


	for (int a = 0; a < vertex->degree; a++)
	{
		for (int v = 0; v < rede->nvertices; v++)
		{
			//Se a aresta do grafo for igual a aresta conexa, entao adiciona aos conexos
			if (grafo[v].id == arestas[a].target)
			{
				conexos = adiciona_vertices(conexos, (grafo + arestas[a].target));
			}
		}
	}
	return conexos;
}

VERTEX *adiciona_vertices(VERTEX *conexos, VERTEX *vertice)
{
	VERTEX *ultimaPosicao;
	if (conexos == NULL)
	{
		conexos = (VERTEX *)malloc(sizeof(VERTEX *));
	}
	else
	{
		conexos = (VERTEX *)realloc(conexos, sizeof(conexos) + sizeof(VERTEX *));
	}
	ultimaPosicao = conexos - sizeof(VERTEX *);
	*(ultimaPosicao) = *(vertice);
	return conexos;
}