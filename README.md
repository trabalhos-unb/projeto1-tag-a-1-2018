# Projeto 1 - Teoria e Aplicação de Grafos - Turma A - 1/2018

### Bruno Helder - 15/0120338
### Danilo Santos - 14/0135910

---

### Caso queira compilar e executar para testar, utilize, respectivamente:
```
$ cd src
$ make
$ ./projeto1
```

---

*Versão do compilador utilizado:* gcc (Ubuntu 5.4.0-6ubuntu1~16.04.9) 5.4.0 20160609


---

*Os dados utilizados foram extraídos de W. W. Zachary, An
information flow model for conflict and fission in small groups, Journal of
Anthropological Research 33, 452-473 (1977)*

